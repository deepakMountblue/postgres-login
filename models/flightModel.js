module.exports = (sequelize, Sequelize) => {
    const airline = sequelize.define('Airlines', {
      id: {
        type: Sequelize.STRING,
        unique: true,
        primaryKey: true
      },
      name: {
        type: Sequelize.TEXT
      },
      url: {
        type: Sequelize.TEXT
      }
    },  
    {
      timestamps: false
    })
  
    return airline
  }
  