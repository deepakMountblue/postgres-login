module.exports = (sequelize, Sequelize) => {
    const trip = sequelize.define('trip', {
      id: {
        type: Sequelize.STRING,
        unique: true,
        primaryKey: true
      },
      from: {
        type: Sequelize.STRING
      },
      to: {
        type: Sequelize.STRING
      },
      price: {
        type: Sequelize.FLOAT
      },
      departAt: {
        type: Sequelize.DATE
      },
      arriveAt: {
        type: Sequelize.DATE
      },
      noofStops: {
        type: Sequelize.INTEGER
      }
    },
    {
      timestamps: false
    })
  
    return trip
  }
  