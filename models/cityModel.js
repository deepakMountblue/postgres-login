module.exports = (sequelize, Sequelize) => {
    const place = sequelize.define('city', {
      id: {
        type: Sequelize.STRING,
        unique: true,
        primaryKey: true
      },
      pincode: {
        type: Sequelize.TEXT
      },
      city: {
        type: Sequelize.TEXT
      },
      name:{
          type:Sequelize.TEXT
      }
    },
    {
      timestamps: false
    })
    return place
  }
  