module.exports = (sequelize, Sequelize) => {
    const Model = sequelize.define("Users", {
      fname: {
        type: Sequelize.STRING
      },
      lname:{
          type: Sequelize.STRING
      },
      email:{
          type:Sequelize.STRING
      },
      password:{
          type:Sequelize.STRING
      }
      
    });
  
    return Model;
  };