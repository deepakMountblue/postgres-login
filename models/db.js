const dbConfig = require("./dbConfig.js");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,
  operatorsAliases: false,

  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle
  }
});

const db = {};

db.userModel = require("./userModel")(sequelize, Sequelize);
db.cityModel = require("./cityModel")(sequelize, Sequelize);
db.flightModel = require("./flightModel")(sequelize, Sequelize);
db.tripModel = require("./tripModel")(sequelize, Sequelize);

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.tripModel.belongsTo(db.flightModel,{ foreignKey: 'airlineId' })


module.exports = db;
