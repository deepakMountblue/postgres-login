const express = require('express')
const app = express()

const db = require("./models/db");

const PORT = process.env.PORT || 5000
const UserRouter = require('./router/userRouter')
const FlightRouter =require('./router/flightRoute')

require('dotenv').config()

const cors = require('cors')

app.use(cors())
// app.options('*', cors());

db.sequelize.sync()

app.use(express.json())
app.use(express.urlencoded({ extended: false }))

app.use('/user', UserRouter)
app.use('/flight', FlightRouter)
app.get('/', (req, res) => {
    res.json({
        success: "true"
    }).end()
})

app.use((req, res, next) => {
    const error = new Error('Page Not found');
    error.status = 404;
    next(error);
});

app.use((err, req, res, next) => {
    res.locals.message = err.message;
    res.locals.error = err;

    res.status(err.status || 500).json({
        err: true,
        message: err.message
    }).end();
});

app.listen(PORT, () => {
    console.log("listening to PORT " + PORT)
})