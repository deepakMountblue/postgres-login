const tripData=[{
    id: 1,
    from: 4,
    to: 2,
    price: 7762.56,
    departAt: new Date("2021-05-29 11:30:00"),
    arriveAt: new Date("2021-05-29 16:30:00"),
    noofStops: 0,
    airlineId: 3
  },
  {
    id: 2,
    from: 3,
    to: 1,
    price: 5229.91,
    departAt: new Date("2021-05-29 01:30:00"),
    arriveAt: new Date("2021-05-29 06:30:00"),
    noofStops: 2,
    airlineId: 1
  },
  {
    id: 3,
    from: 7,
    to: 3,
    price: 4249.59,
    departAt: new Date("2021-05-28 20:30:00"),
    arriveAt: new Date("2021-05-29 00:30:00"),
    noofStops: 1,
    airlineId: 4
  },
  {
    id: 4,
    from: 10,
    to: 2,
    price: 6597.46,
    departAt: new Date("2021-05-29 08:30:00"),
    arriveAt: new Date("2021-05-29 13:30:00"),
    noofStops: 2,
    airlineId: 3
  },
  {
    id: 5,
    from: 1,
    to: 1,
    price: 6271.42,
    departAt: new Date("2021-05-29 22:30:00"),
    arriveAt: new Date("2021-05-30 02:30:00"),
    noofStops: 1,
    airlineId: 4
  },
  {
    id: 6,
    from: 3,
    to: 3,
    price: 7554.06,
    departAt: new Date("2021-05-29 06:30:00"),
    arriveAt: new Date("2021-05-29 11:30:00"),
    noofStops: 0,
    airlineId: 3
  },
  {
    id: 7,
    from: 3,
    to: 1,
    price: 3437.13,
    departAt: new Date("2021-05-30 16:30:00"),
    arriveAt: new Date("2021-05-30 20:30:00"),
    noofStops: 2,
    airlineId: 4
  },
  {
    id: 8,
    from: 2,
    to: 1,
    price: 5955.92,
    departAt: new Date("2021-05-29 08:30:00"),
    arriveAt: new Date("2021-05-29 11:30:00"),
    noofStops: 1,
    airlineId: 1
  },
  {
    id: 9,
    from: 3,
    to: 2,
    price: 9176.16,
    departAt: new Date("2021-05-29 07:30:00"),
    arriveAt: new Date("2021-05-29 12:30:00"),
    noofStops: 1,
    airlineId: 4
  },
  {
    id: 10,
    from: 2,
    to: 2,
    price: 2954.39,
    departAt: new Date("2021-05-30 06:30:00"),
    arriveAt: new Date("2021-05-30 08:30:00"),
    noofStops: 2,
    airlineId: 1
  }]
module.exports= tripData