const airlineData = [
    {
      id: '1',
      name: 'IndiGo',
      url: 'https://imgak.mmtcdn.com/flights/assets/media/dt/common/icons/6E.png?v=5'
    },
    {
      id: '2',
      name: 'SpiceJet',
      url: 'https://imgak.mmtcdn.com/flights/assets/media/dt/common/icons/G8.png?v=5'
    },
    {
      id: '3',
      name: 'GoAir',
      url: 'https://imgak.mmtcdn.com/flights/assets/media/dt/common/icons/G8.png?v=5'
    },
    {
      id: '4',
      name: 'Vistara',
      url: 'https://imgak.mmtcdn.com/flights/assets/media/dt/common/icons/UK.png?v=5'
    },
    {
      id: '5',
      name: 'AirAsia',
      url: 'https://imgak.mmtcdn.com/flights/assets/media/dt/common/icons/I5.png?v=5'
    }
  ]
  

module.exports = airlineData
