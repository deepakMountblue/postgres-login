const placeData = [
    {
      id: '1',
      name: 'Dr. Babasaheb Ambedkar International Airport',
      city: 'Nagpur',
      pincode: '400000'
    },
    {
      id: '2',
      name: 'Chhatrapati Shivaji International Airport',
      city: 'Mumbai',
      pincode: '400000',
    },
    {
      id: '3',
      name: 'Bengaluru International Airport',
      city: 'Bengaluru',
      pincode: '400000',
    },
    {
      id: '4',
      name: 'Tulihal International Runway',
      city: 'Imphal',
      pincode: '400000',
    },
    {
      id: '5',
      name: 'Biju Patnaik International Airport',
      city: 'Bhubaneshwar',
      pincode: '400000',
    },
    {
      id: '6',
      name: 'Sri Guru Ram Dass Jee International Airport',
      city: 'Amritsar',
      pincode: '400000',
    },
    {
      id: '7',
      name: 'Jaipur International Airport',
      city: 'Jaipur',
      pincode: '400000',
    },
    {
      id: '8',
      name: 'Chennai International Airstrip',
      city: 'Chennai',
      pincode: '400000',
    },
    {
      id: '9',
      name: 'Coimbatore International Airport',
      city: 'Coimbatore',
      pincode: '400000',
    },{
        id: '10',
        name: 'Rajiv Gandhi International Airport',
        city: 'Hyderabad',
        pincode: '400000',
      }
    
  ]
  module.exports = placeData
  