const router = require('express').Router()

const db = require('../models/db')
var jwt = require('jsonwebtoken');
const auth = require('../auth');
const Op = db.Sequelize.Op
require('dotenv').config()

const UserModel = db.userModel
function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

router.get('/', auth, (req, res) => {
    res.json({
        success: "true"
    }).end()
})

router.post('/register', (req, res, next) => {

    const inputData = {
        fname: req.body.fname,
        lname: req.body.lname,
        email: req.body.email,
        password: req.body.password,
    }

    if (!inputData.fname || !inputData.lname || !inputData.email || !inputData.password) {
        console.log(inputData)
        res.status(400).json({
            err: true,
            message: "all fields are required!"
        }).end();
    }
    else {
        if (!validateEmail(inputData.email)) {
            res.status(400).json({
                err: true,
                message: "Not a valid email!"
            }).end();
        }
        else {
            UserModel.findOne({
                where: {
                    email: inputData.email
                }
            })
                .then(data => {
                    console.log(data)
                    if (data) {
                        // check unique email address
                        var msg = inputData.email + " was already exist";
                        res.status(400).json({
                            err: true,
                            message: msg
                        }).end()
                    }
                    else if (inputData.password === undefined) {
                        var msg = "Password should not be empty";
                        res.status(400).json({
                            err: true,
                            message: msg
                        }).end()
                    }
                    else if (req.body.confirm_password !== inputData.password) {
                        var msg = "Passwords not Matched";
                        res.status(400).json({
                            err: true,
                            message: msg
                        }).end()
                    }
                    else {
                        // save users data into database
                        UserModel.create(inputData).then(data => {
                            res.status(200).json(data).end();
                        })
                            .catch(err => {
                                next(err)
                            });
                    }

                })
                .catch(Err => {
                    console.log("erroror")
                    next(Err)
                })
        }
    }
})


router.post('/login', (req, res, next) => {

    if (req.body.password && req.body.email) {
        var email = req.body.email;
        var password = req.body.password;
        if (validateEmail(email)) {

            UserModel.findOne({
                where: {
                    email,
                    password
                }
            })
                .then(data => {
                    if (data) {

                        const token = jwt.sign({
                            email,
                            password
                        }, process.env.SECRET);
                        res.status(200).send({
                            token,
                            name: data.fname + " " + data.lname
                        })
                    }
                    else {
                        res.status(200).json({
                            err: true,
                            message: "Email or password is not correct!"
                        }).end()
                    }
                })
                .catch(err => {
                    next(err)
                })
        }
        else {
            res.status(400).json({
                err: true,
                message: "not a valid email!"
            }).end()
        }
    }
    else {
        res.status(400).json({
            err: true,
            message: "All fields are necessary"
        })
    }
})

router.get('/find', auth, ((req, res, next) => {
    UserModel.findAll({}).then(data => {
        res.status(200).json(data).end()
    }).catch(Err => {
        next(Err)
    })
}))



module.exports = router