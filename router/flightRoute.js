const router = require('express').Router()

const db = require('../models/db')
const Op = db.Sequelize.Op

const tripModel = db.tripModel
const cityModel = db.cityModel
const flightModel =db.flightModel

router.get('/', (req, res, next) => {
    res.json({
        msg:"hello"
    })
})
router.post('/getFlights', (req, res, next) => {
    
    const startDate = new Date(req.body.date)
    const nextDay = new Date(startDate)
    nextDay.setDate(nextDay.getDate() + 1)
    console.log(startDate.toLocaleString(),nextDay.toLocaleString())
    const where = {
        departAt: {
            [Op.between]: [startDate, nextDay]
        }
    }
    tripModel.findAll({
        where,
        include: [{ model: flightModel }]
    }).then(data => {
        const dates=data.map(ele=>new Date(ele.departAt).toLocaleDateString())
        res.status(200).json(data).end()
    }).catch(Err => {
        next(Err)
    })
})
router.get("/getCities", (req, res, next) => {

    cityModel.findAll({

    }).then(data => {
        res.status(200).json(data).end()
    }).catch(Err => {
        next(Err)
    })
})

module.exports = router